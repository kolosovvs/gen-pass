import React, { Component } from 'react';
import './App.css';
import Generate from './components/Generate'
import logo from './images/bitbucket.png';

class App extends Component {
  
  render() {
    return (
      <div className="App">
        <h1 className="white">Generate Password</h1>
        <Generate />
        <a target="_blank" href="https://bitbucket.org/kolosovvs/gen-pass/">
          <img src={logo} className="bitbucket-logo" alt="bitbucket logo" />
        </a>
      </div>
    );
  }
}

export default App;
