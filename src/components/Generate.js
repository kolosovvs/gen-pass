import React, {Component} from 'react';
import "./Generate.css"
import "../App.css"

class Generate extends Component {

  state = {
    password_list: [],
    passlenght: 0,
    countpass: 0,
  };

  randomsString(L) {
    let s = '';
    let randomchar = function() {
      let n = Math.floor(Math.random() * 62);
      if (n < 10) return n; //1-10
      if (n < 36) return String.fromCharCode(n + 55); //A-Z
      return String.fromCharCode(n + 61); //a-z
    }
    while (s.length < L) s += randomchar();
    return s;
  }

  generatePasswords = () => {
      let password_list = []
      for(let i=0; i<this.state.countpass; i++){
        password_list.push(this.randomsString(this.state.passlenght))
      }
      
      this.setState({password_list: password_list})
    }
  
    

    handleChange = (event) => {
      let { name, value } = event.target;
      this.setState({[name] : value});
    }
  

  render() {

    let passList = this.state.password_list.map((pass, index) =>
      <li key={index} className="mt-2">{pass}</li>
      
    )

    let message = ''
    if (this.state.password_list.length){
      message = 'Here are your random passwords:'
    }

    return (
      <div className="generate-component">
        <div className="pure-form pure-form-aligned form">
          <label htmlFor="passlenght" className="white">Password length </label>
          <input name="passlenght" onChange={this.handleChange} type="text" placeholder="Password length" />
          <label htmlFor="countpass" className="white">Count password </label>
          <input name="countpass" onChange={this.handleChange} type="text" placeholder="Count password" />
          <button className="generate-button pure-button white" onClick={this.generatePasswords}>Generate</button>
        </div>

        <div className="mt-10 passwords white">
          <div className="mb-2">{message}</div>
          <ul>
            {passList}
          </ul>
        </div>
        
      </div>
    );
  }
}

export default Generate;